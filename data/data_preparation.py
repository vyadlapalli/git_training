def summary(dataset):
    # returns a pandas dataframe object
    import pandas as pd
    df = pd.read_csv(dataset)
    return df.describe()

def summary(df):
    return df.describe()

